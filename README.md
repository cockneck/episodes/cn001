# The Cockneck Manifesto
---
## Setup
1. Download and install Twine.
2. Upgrade Twine's version of SugarCube using the instructions here: https://www.motoslave.net/sugarcube/2/docs/#guide-installation-twine2
3. Clone this repo.
4. In Twine, choose "Import From File" and select the index.html file inside of the public folder.
5. You can now open and edit the story. Click on "Untitled Brain Story" in the bottom left of Twine and choose "Publish To File" to save over public/index.html.
